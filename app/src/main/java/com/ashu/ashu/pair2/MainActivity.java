package com.ashu.ashu.pair2;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String FILE_NAME = "map.txt";
    TextView myLabel;
    TextView myView;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    String decimal;
    ArrayList<String> d=new ArrayList<String>();
    ArrayList<String> d1=new ArrayList<String>();
    int[] sendbuff = new int[33];
    int i;
    int r;
    int p=16;
    int cs=0;
    int readBufferPosition;
    int counter=0;
    int mapstart;
    int mapend;
    int adr=mapstart;
    int eadr=mapend;
    volatile boolean stopWorker;
    String fileName = Environment.getExternalStorageDirectory()+ "/map.txt";
    String actualFile = fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button openButton = (Button)findViewById(R.id.open);
        Button sendButton = (Button)findViewById(R.id.send);
        Button closeButton = (Button)findViewById(R.id.close);
        myLabel = (TextView)findViewById(R.id.label);
        myView =(TextView) findViewById(R.id.show);
        createTextFile(actualFile);
        //Connect Bluetooth
        openButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try
                {
                    findBT();
                    openBT();
                }
                catch (IOException ignored) { }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try {
                    secondData();
                    Intent intent=new Intent(MainActivity.this,LoadFileActivity.class);
                    startActivity(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //call
            }
        });
        //Close Bluetooth connection
        closeButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try {
                    closeBT();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    void findBT()
    {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null)
        {
            myLabel.setText("No Bluetooth adapter available");
        }
        if(!mBluetoothAdapter.isEnabled())
        {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        pairedDevices.size();
        for(BluetoothDevice device : pairedDevices)
        {
            if(device.getName().equals("HC-05"))
            {
                mmDevice = device;
                break;
            }else{mmDevice = device;}
        }
        myLabel.setText("Bluetooth Device Not Found");
    }

    void openBT() throws IOException
    {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();
        beginListenForData();
        myLabel.setText("Bluetooth Connected");
        sendData();
    }
    void closeBT() throws IOException
    {
        stopWorker = true;
        mmOutputStream.close();
        mmInputStream.close();
        mmSocket.close();
        myLabel.setText("Bluetooth Disconnected");
    }

    void beginListenForData()
    {
        final Handler handler = new Handler();
        final byte delimiter = (byte) 50000; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[50000];
        workerThread = new Thread(new Runnable()
        {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            d.add(data);//= data;
                                            myView.setText(data);
                                        }
                                    });
                                }
                                else
                                {
                                    d.clear();
                                    readBuffer[readBufferPosition++] = b;
                                    byte[] encodedBytes = new byte[readBufferPosition];

                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    final BigInteger bi = new BigInteger(encodedBytes);
                                    // Format to decimal
                                    decimal = bi.toString();

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            d.add(String.valueOf(bi));//= data;
                                            d1.add(data);
                                             //  String list= ArrayList.toString(d1.toArray()).replace("["," ").replace("]"," ");
                                            String list= d1.toString().replace("["," ").replace("]"," ").replace(",","");
                                       //     myView.setText(String.valueOf(d));
                                            updateTextFile(actualFile,list);
                                        }
                                    });
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        stopWorker = true;
                    }
                }
            }
        });
        workerThread.start();
    }
    public void secondData() throws IOException {
       /* int a= Integer.parseInt(d.get(4));
        int b=256*Integer.parseInt(d.get(5));
        int c=65536*Integer.parseInt(d.get(6));
        int e=256*(65536*Integer.parseInt(d.get(7)));

        mapend=e+c+b+a;
      /*  mapstart= (int) (Integer.parseInt(d.get(0))+(256*Integer.parseInt(d.get(1)))+(65536*Integer.parseInt(d.get(2)))+(256*65536*Integer.parseInt(d.get(3))));
       Toast.makeText(this, "mapstart"+mapstart, Toast.LENGTH_SHORT).show();

       mapend= (int)(Integer.parseInt(d.get(4)))+(256*Integer.parseInt(d.get(5)))+(65536*Integer.parseInt(d.get(6)))+(256*65536*Integer.parseInt(d.get(7))));*/
        //Toast.makeText(this, "mapend"+mapend, Toast.LENGTH_SHORT).show();
      //  Toast.makeText(this, "map file Downloaded...", Toast.LENGTH_SHORT).show();


      //here 2nd que
        sendbuff[0]=0xE0;
        sendbuff[1]=0xB2;
        sendbuff[2]=0xC4;
        sendbuff[3]=33;
        sendbuff[4]=0;
        sendbuff[5]=0;
        sendbuff[6]=0xFF;
        sendbuff[7]=0xFF;
        sendbuff[8]=0xFF;
        sendbuff[9]=0xFF;
        sendbuff[10]=0xFF;
        sendbuff[11]=0xFF;
        sendbuff[12]=0xFF;
        sendbuff[13]=0xFF;
        sendbuff[14]=0;
        sendbuff[15]=headercs(sendbuff);
        sendbuff[16]=0x0C;
        sendbuff[17]=0;
        sendbuff[18]=0;
        sendbuff[19]=8;
        sendbuff[20]=5;
        sendbuff[21]=0X11;
        sendbuff[22]=1;
        sendbuff[23]=0;
        sendbuff[24]=0;
        sendbuff[25]=0xFF;
        sendbuff[26]=0X03;
        sendbuff[27]=0;
        sendbuff[28]=0;
        sendbuff[29]=0x03;
        sendbuff[30]=0x4A;
        sendbuff[31]=0x7F;
        sendbuff[32]=(blkcs(sendbuff,31));


       /* sendbuff[26]=(adr/65536)&0xFF;
        sendbuff[27]=(adr)&0xFF;
        sendbuff[28]=(adr/256)&0xFF;
        sendbuff[29]=(eadr/65536)&0xFF;
        sendbuff[30]=(eadr)&0xFF;
        sendbuff[31]=(eadr/256)&0xFF;*/
        Toast.makeText(this, "Map file Downloaded...", Toast.LENGTH_SHORT).show();

        for (int i1 : sendbuff) {
            mmOutputStream.write(i1);
        }
    }

    public int headercs(int[] sendbuf)
    {
        r=0;
        for(i=0; i<15; ++i)r=r^ sendbuf[i];
        return r;
    }

    public int blkcs(int[] sendbuff, int i)
    {
        while(p<=i)
        { cs=cs^ sendbuff[p];
            ++p;
        }
        return cs;
    }

    void sendData() throws IOException
    {
        mmOutputStream.write(0xE0);
        mmOutputStream.write(0xB2);
        mmOutputStream.write(0xC4);
        mmOutputStream.write(0x1A);
        mmOutputStream.write(0x00);
        mmOutputStream.write(0x00);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0xFF);
        mmOutputStream.write(0x00);
        mmOutputStream.write(0x8C);
        mmOutputStream.write(0x06);
        mmOutputStream.write(0x01);
        mmOutputStream.write(0x01);
        mmOutputStream.write(0x08);
        mmOutputStream.write(0x05);
        mmOutputStream.write(0x11);
        mmOutputStream.write(0x01);
        mmOutputStream.write(0x04);
        mmOutputStream.write(0x04);
        mmOutputStream.write(0x1B);
        myLabel.setText("Data Sent");
    }

    //Create a text file.
    public void createTextFile(String actualFile) {
        try {
            File file = new File(actualFile);
            if (file.exists()) {
                Toast.makeText(this, "Map file already exists.", Toast.LENGTH_SHORT).show();
            } else {
                // create the text file
                if (file.createNewFile()) {
                    Toast.makeText(this, "Map file was created.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Unable to create text file.", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Update a text file.
    public void updateTextFile(String actualFile, String contents) {
        try {
            File textFile = new File(actualFile);

            if (textFile.exists()) {

                FileWriter textFileWriter = new FileWriter(textFile, false);
                BufferedWriter out = new BufferedWriter(textFileWriter);
                // write the updated content
                out.write(String.valueOf(contents));
                Toast.makeText(this, "Get info Downloaded...", Toast.LENGTH_SHORT).show();
                out.close();
            } else {
                Toast.makeText(this, "Cannot update. File does not exist.", Toast.LENGTH_SHORT).show();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}