package com.ashu.ashu.pair2;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class MainnActivity extends AppCompatActivity {
    ListView listView;


    private static final int READ_REQUEST_CODE = 42;
    private static final int PERMISSION_REQUEST_STORAGE = 1000;
    String dummy;
    Button b_load;
    TextView tv_output;
   ArrayAdapter<String> arrayadapter;
   // SearchView searchView;
    EditText editText;
    // BufferedReader bufferedReaderCounter;
    //int intCount = 0;

    ArrayList<String> arrayList;
    String line;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainn);

        tv_output = (TextView) findViewById(R.id.tv_output);
        listView=(ListView) findViewById(R.id.listView);

      arrayList = new ArrayList<String>(Arrays.asList(line));
      arrayadapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.textView, Collections.singletonList(line));
        listView.setTextFilterEnabled(true);
      // listView.setAdapter(arrayadapter);
        editText=(EditText)findViewById(R.id.editText);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // Toast.makeText(getApplicationContext(), map[position] + "was clicked", Toast.LENGTH_SHORT).show()

                String text = listView.getItemAtPosition(position).toString();
                Toast.makeText(MainnActivity.this, "" + text, Toast.LENGTH_SHORT).show();
               // dummy = "  ";     //substring containing first 4 characters

                if (text.length() >= 6) {
                    dummy = text.substring(0, 12);
                    tv_output.setText(dummy);
                    // textView1.setText(String.valueOf(dummy));
                    //textView2.setText(String.valueOf(dummy));
                } else {
                    dummy = text;
                }
                Toast.makeText(getApplicationContext(), "variable stored", Toast.LENGTH_LONG).show();
                //String itemValue = (String) listView.getItemAtPosition(position);
            }
        });

    //request permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);
        }

        b_load = findViewById(R.id.b_load);
        b_load.setOnClickListener(new View.OnClickListener() {
            // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                performFileSearch();

            }
        });
    }

    //read the content of file


    /*File file = new File(Environment.getExternalStorageDirectory(), input);
    StringBuilder text = new StringBuilder();
    try {

        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;

        while ((line = br.readLine()) != null) {
            text.append(line);
            text.append("\n");

        }


        br.close();

    } catch (IOException e) {

        e.printStackTrace();
    }
    return text.toString();


}*/
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static List<String> readFileIntoList(String file) {
        List<String> lines = Collections.emptyList();
        try {
            lines = Files.readAllLines(Paths.get(file), StandardCharsets.UTF_8);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lines;
    }


    //Read more: http://www.java67.com/2016/07/how-to-read-text-file-into-arraylist-in-java.html#ixzz5psXxHxd9


    //select file from storage
    // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                assert uri != null;
                String path = uri.getPath();
                assert path != null;
                path = path.substring(path.indexOf(":") + 1);
                if (path.contains("emulated")) {
                    path = path.substring(path.indexOf("0") + 1);
                }

                Toast.makeText(this, "" + path, Toast.LENGTH_SHORT).show();

                try {
                    String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/map.txt";
                    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"), 100);
                    //String line;
                    ArrayList<String> lines = new ArrayList<String>();
                    while ((line = br.readLine()) != null) {
                        lines.add(line);
                    }
                    br.close();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lines);
                    listView.setAdapter(adapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged (CharSequence charSequence,int i, int i1, int i2){
                    }

                    @Override
                    public void onTextChanged (CharSequence charSequence,int i, int i1, int i2){
                        MainnActivity.this.arrayadapter.getFilter().filter(charSequence);
                        arrayadapter.notifyDataSetChanged();
                    }
                    @Override
                    public void afterTextChanged (Editable charSequence){
                    }
                });

                //tv_output.setText(readFileIntoList(String file));
                //ArrayAdapter<String> arrayadapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lines);
                // listView.setAdapter(arrayadapter);
                //arrayadapter.add(file);
                // listView.addView(arrayadapter);
                // listView.add();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.item_search);
       // SearchView searchView = (SearchView) MenuItemCompat.getTooltipText(searchItem);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));

        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<String> templist = new ArrayList<String>();

                String[] line = new String[0];

                for (String temp : line) {
                    if (temp.toLowerCase().contains(newText.toLowerCase())) {
                        templist.add(temp);
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainnActivity.this, android.R.layout.simple_list_item_1, templist);
                listView.setAdapter(adapter);

                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }*/

  }