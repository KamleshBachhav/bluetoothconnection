package com.ashu.ashu.pair2;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LoadFileActivity extends AppCompatActivity {

    volatile boolean stopWorker;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    BluetoothSocket mmSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_file);
        Button disconnect=findViewById(R.id.disconnect);
        Button read=findViewById(R.id.read);
        Button load=findViewById(R.id.load);

        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(LoadFileActivity.this,OutputActivity.class);
                startActivity(intent);
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(LoadFileActivity.this,MainnActivity.class);
                startActivity(intent);
            }
        });

        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*try {
                    closeBT();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }
        });

    }

    void closeBT() throws IOException
    {
        stopWorker = true;
        mmOutputStream.close();
        mmInputStream.close();
        mmSocket.close();
        //myLabel.setText("Bluetooth Disconnected");
    }
}
