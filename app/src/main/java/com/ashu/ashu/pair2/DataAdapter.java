package com.ashu.ashu.pair2;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class DataAdapter extends ArrayAdapter {

    Context context;
    int resource;
    ArrayList<String> myList;

    public DataAdapter(Context context, int resource, ArrayList<String> objects){
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        myList = objects;
    }
    @Override
    public View getView(int position, View ConvertView, ViewGroup parent){
        View myView = null;

        myView = LayoutInflater.from(context).inflate(resource,parent,false);
        //TextView  txtName = (TextView)myView.findViewById(R.id.tv_output);
        ListView listView =(ListView)myView.findViewById(R.id.listView);
        String dataName = myList.get(position);
       // ArrayAdapter<String> arrayadapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dataName);
       // listView.setAdapter(arrayadapter);

        //txtName.setText(dataName);


        return myView;
    }

}
